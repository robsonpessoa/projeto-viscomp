# Visualização Computacional

Este repositório foi criado para fins acadêmicos para a disciplina 
de Visualização Computacional do Bacharelado em Ciências de Computação
e Bacharelado em Ciências de Dados da Universidade de São Paulo, São Carlos/SP.

O dataset do Projeto Prático escolhido foi do Spotify, chamado [Most Streamed Spotify Songs 2023](https://www.kaggle.com/datasets/nelgiriyewithana/top-spotify-songs-2023).

## Participantes
Mavi
Pedro Fiddo
Robson Pessoa